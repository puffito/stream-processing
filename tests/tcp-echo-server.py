from time import sleep
import socket

data = 'abcdefg\n'
to_be_sent = data.encode('utf-8')


# Create a socket with the socket() system call
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind the socket to an address using the bind() system call
s.bind(('localhost', 9999))
s.listen(1)

# Catch the incoming TCP connection from spark and accept it
conn, addr = s.accept()
print(f"Connection established at {addr}")

while True:
    # Until bored, send mails to spark
    conn.sendall(to_be_sent)
    print("data sent")
    print("sleeping")
    sleep(1)
