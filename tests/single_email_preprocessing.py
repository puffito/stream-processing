import re
import sys
from collections import Counter
from time import perf_counter

import nltk
import numpy as np
import pandas as pd
from nltk.corpus import stopwords, wordnet

"""
Preprocesses a mail in a txt file and tokenizes it. 
It's purpose was to see if the spark streaming algorithm optimized 
and tokenized the same email in the exact same way.
"""

DATA_LOCATION = 'D:\\Google Drive\\Coding\\Arbeit\\streaming processing\\dataset\\newham\\0001.ea7e79d3153e7469e7a9c3e0af6a357e'

try:
    email = open(DATA_LOCATION, "r", encoding="utf8").read()
    print("Imported Email")
    print(email)
except(FileNotFoundError):
    print("File could not be found")
    exit(-1)

print("Optimizing")
email = re.sub('[^a-zA-Z]+', ' ', email).lower().split()
print(email)

stop_words = set(stopwords.words('english'))
email = [word for word in email if not word in stop_words]

wnl = nltk.WordNetLemmatizer()
email = [wnl.lemmatize((wnl.lemmatize(word, pos='n')), pos='v')
         for word in email]

print("Creating Dataframes")
counter_words = Counter(email)
print(counter_words)

# Creata sorted and indexed pandas Dataframe containg the words and their counts
words_df = pd.DataFrame.from_dict(data=counter_words, orient='index').reset_index().\
    rename(columns={'index': 'word', 0: 'count'}).sort_values(
        by=['word']).reset_index(drop=True)
print(words_df)
