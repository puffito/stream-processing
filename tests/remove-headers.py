# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import os
def remove_headers(lines: list) -> str:
    """ Takes a list of lines and returns a string. The code finds the find line that is empty, and 
    returns a string containg the remainder of the lines in a human-readeable format 
    """
    
    counter = 0
    for line in lines:
        if line == '':
            newtext = " ".join(lines[counter:])
            return newtext
        counter += 1
    # in case parsing failed, return original text
    return " ".join(lines)


# %%
# open each file in the spam and the hame directory and apply remove_headers
# save the output in seperate folders

spam_files = os.listdir('../dataset/spam')
ham_files = os.listdir('../dataset/ham')

for filename in spam_files:
    with open(file=f'../dataset/spam/{filename}', mode='r', errors='ignore') as file:
        # important to open the new file with utf-8 encoding, so as to not break encoding
        newfile = open(file=f'../dataset/newspam/{filename}', mode='w', encoding='utf-8')
        try:
            newfile.write(remove_headers(file.read().splitlines()))
            newfile.close()
        except UnicodeDecodeError:
            print(filename)
            newfile.close()


# %%
for filename in ham_files:
    with open(file=f'../dataset/ham/{filename}', mode='r', errors='ignore') as file:
        # important to open the new file with utf-8 encoding, so as to not break encoding
        newfile = open(file=f'../dataset/newham/{filename}', mode='w', encoding='utf-8')
        try:
            newfile.write(remove_headers(file.read().splitlines()))
            newfile.close()
        except UnicodeDecodeError:
            print(filename)
            newfile.close()


# %%


