from pyspark.sql import SparkSession
from pyspark.sql.functions import col, explode, lower, regexp_replace, split
from pyspark.sql.types import StringType, StructField, StructType

# initiate a spark session
session = SparkSession.builder.\
    appName("File Wordcount Structured").\
    master("local[*]").\
    getOrCreate()

# set LOG level
session.sparkContext.setLogLevel("ERROR")

# read incoming text files placed in watch folder, each row of the text file is a row in the dataframe
data = session.readStream\
    .format("text")\
    .load("datasets/dropfolder")

# select everything from the database - needed to be able to output as stream
query = data.select("*")

# print out selection
query.writeStream\
    .format("console")\
    .start()\
    .awaitTermination()
