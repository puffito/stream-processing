from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    # use all local cores
    sc = SparkContext("local[*]", appName="PythonStreamingNetworkWordCount")
    ssc = StreamingContext(sc, 5)
    
    # monitor specific directory and...
    lines = ssc.textFileStream("/d/")

    # break down the file into words
    counts = lines.flatMap(lambda line: line.split(" ")) \
                  .map(lambda x: (x, 1))\
                  .reduceByKey(lambda a, b: a+b)
    
    # print result to stdout
    counts.pprint()
    
    # start stream
    ssc.start()
    ssc.awaitTermination()
