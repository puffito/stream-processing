from pyspark.sql import SparkSession
from pyspark.sql.functions import col, explode, lower, regexp_replace, split

# initiate a spark session
session = SparkSession.builder.\
    appName("Socket Wordcount Structured").\
    master("local[*]").\
    getOrCreate()

session.sparkContext.setLogLevel("ERROR")

# loads the incoming socket datastream to a DataFrame instance
data = session.readStream.\
    format("socket").\
    options(host="localhost", port="9000").\
    load()

# split the incoming words, lowercase them, remove special characters
words = data.select(explode(split(data.value, " ")).alias("words")).\
    withColumn("words", lower(col("words"))).\
    withColumn("words", regexp_replace(col("words"), "[^a-zA-Z0-9]+", ""))

# group them by word and aggregate their word counts
counts = words.groupBy("words").count().orderBy("count", ascending=False)

# print the dataframe
output = counts.writeStream.format("console").outputMode(
    "complete").option("numRows", 10).start().awaitTermination()
