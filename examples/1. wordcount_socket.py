from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: network_wordcount.py <hostname> <port>", file=sys.stderr)
        sys.exit(-1)
    # use all local cores
    sc = SparkContext("local[*]", appName="PythonStreamingNetworkWordCount")
    sc.setLogLevel("WARN")

    # 1 second between checks, batch interval 
    ssc = StreamingContext(sc, 1)
    # grab incoming lines pointed to a certain socket
    lines = ssc.socketTextStream(sys.argv[1], int(sys.argv[2]))
    counts = lines.flatMap(lambda line: line.split(" "))\
                  .map(lambda word: (word, 1))\
                  .reduceByKey(lambda a, b: a+b)
    counts.pprint()

    ssc.start()
    ssc.awaitTermination()
