import os
import re
import signal
import sys
from time import sleep

import findspark
from pyspark import SparkConf, SparkContext
from pyspark.rdd import RDD
from pyspark.sql import SparkSession
from pyspark.streaming import StreamingContext

from probability import Probability
from train import (
    ham, missing_probability, mprob_email_ham, mprob_email_spam, spam,
    stop_words, wnl)

findspark.init()

#! program must be run from src/ directory!
# create spark context and streaming context
sc = SparkContext("local[*]", "Union of streams")
ssc = StreamingContext(sc, 3)
ssc.sparkContext.setLogLevel('FATAL')

#  accumulator
spam_acc = sc.accumulator(0)
ham_acc = sc.accumulator(0)
# monitoring sockets
lines_0 = ssc.socketTextStream("localhost", 9999).map(lambda x: x.split('\n'))
lines_1 = ssc.socketTextStream("localhost", 9998).map(lambda x: x.split('\n'))

words_0 = lines_0.map(lambda line:
                      re.sub('[^a-zA-Z]+', ' ', line[0]).
                      lower().
                      split()).\
    map(lambda line: [word for word in line if word not in stop_words]).\
    map(lambda line: [wnl.lemmatize((wnl.lemmatize(word, pos='n')), pos='v') for word in line])

words_1 = lines_1.map(lambda line:
                      re.sub('[^a-zA-Z]+', ' ', line[0]).
                      lower().
                      split()).\
    map(lambda line: [word for word in line if word not in stop_words]).\
    map(lambda line: [wnl.lemmatize((wnl.lemmatize(word, pos='n')), pos='v') for word in line])

# Union of preprocessed emails
words = words_0.union(words_1)

def classify(mails: RDD):
    """
    Classifies the given mail contained in an rdd.
    """
    mails = list(filter(lambda mail: len(mail) > 0, mails.collect()))
    if len(mails) > 0:
        for mail in mails:
            for word in mail:
                # Calculate spam
                if spam['word'].isin([word]).sum() > 0:
                    mprob_email_spam.multiply(
                        spam[spam.word == word].values[0][2])
                else:
                    mprob_email_spam.multiply(missing_probability)
                # Calculate ham
                if ham['word'].isin([word]).sum() > 0:
                    mprob_email_ham.multiply(ham[ham.word == word].values[0][2])
                else:
                    mprob_email_ham.multiply(missing_probability)
            q = mprob_email_ham.calculate_q(mprob_email_spam)
            if q == 0:
                ham_acc.add(1)
                print('ham')
            else:
                spam_acc.add(1)
                print('spam')
            mprob_email_ham.reset()
            mprob_email_spam.reset()

def terminate_process(signalNumber, frame):
    print('Got SIGTERM,terminating the process')
    ssc.stop(stopGraceFully=True, stopSparkContext=True)
    sys.exit()


signal.signal(signal.SIGTERM, terminate_process)
signal.signal(signal.SIGINT, terminate_process)

# Classify the joined stream
words.foreachRDD(classify)

# start stream
print("Starting DStream with PID ", os.getpid())
ssc.start()
ssc.awaitTermination()
