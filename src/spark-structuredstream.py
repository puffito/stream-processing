import os

from nltk import WordNetLemmatizer
from nltk.corpus import stopwords
from pyspark.sql import SparkSession
from pyspark.sql.functions import (col, explode, lower, regexp_replace, split,
                                   udf)
from pyspark.sql.types import ArrayType, IntegerType, StringType

from probability import Probability
from train import (
    ham, missing_probability, mprob_email_ham, mprob_email_spam, spam,
    stop_words, wnl)

# initiate a spark Session
spark = SparkSession.builder.\
    appName("Socket Wordcount Structured").\
    master("local[*]").\
    getOrCreate()

spark.sparkContext.setLogLevel("ERROR")

# loads the incoming socket datastream to a DataFrame instance
print("Starting Structured Stream with PID ", os.getpid())
data = spark.readStream.\
    format("socket").\
    options(host="localhost", port="9999").\
    load()


def remove_stopwords(mail: str) -> list:
    return [word for word in mail.split() if word not in stop_words]


remove_stopwords_udf = udf(remove_stopwords, ArrayType(StringType()))


def lemmatize(words: list):
    return [wnl.lemmatize((wnl.lemmatize(word, pos='n')), pos='v') for word in words]


lemmatize_udf = udf(lemmatize, ArrayType(StringType()))

# break down the stream into optimized words
words = data.select(lower(data.value).alias('mail'))\
            .select(regexp_replace('mail', r'[^A-Za-z]', ' ').alias('mail'))\
            .select(remove_stopwords_udf("mail").alias('mail'))\
            .select(lemmatize_udf("mail").alias("mail"))


def classify(mail: list) -> str:
    """
    Classifies the given mail contained in an rdd.
    """
    if len(mail) > 0:
        for word in mail:
            # Calculate spam
            if spam['word'].isin([word]).sum() > 0:
                mprob_email_spam.multiply(
                    spam[spam.word == word].values[0][2])
            else:
                mprob_email_spam.multiply(missing_probability)
            # Calculate ham
            if ham['word'].isin([word]).sum() > 0:
                mprob_email_ham.multiply(ham[ham.word == word].values[0][2])
            else:
                mprob_email_ham.multiply(missing_probability)
        q = mprob_email_ham.calculate_q(mprob_email_spam)
        q = 'ham' if q == 0 else 'spam'

        # reset probabilites for next email
        mprob_email_ham.reset()
        mprob_email_spam.reset()
        return q
    else:
        print("Nothing to print")


classify_udf = udf(classify, StringType())

words = words.select("mail", classify_udf("mail").alias("classification"))
words = words.groupby("classification").count()

# print the dataframe
output=words.writeStream.format("console")\
    .outputMode("complete")\
    .option("numRows", 10)\
    .start()\
    .awaitTermination()
