import os
import random
import socket
import sys
from time import sleep

# Ensure proper usage
if len(sys.argv) < 3:
    print("Proper usage: send-mails <ports> <rate>")
    sys.exit(-1)

# Code Parameters
HAM_DIR = os.listdir('../dataset/newham/')
SPAM_DIR = os.listdir('../dataset/newspam/')
NUM_HAM = len(HAM_DIR)
NUM_SPAM = len(SPAM_DIR)
PORTS = [int(x) for x in sys.argv[1:-1]]
RATE = int(sys.argv[-1])
SOCKETS = []
CONNECTIONS = []

# Create multiple sockets
for port in PORTS:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(f"Binding port {port}.")
    s.bind(('0.0.0.0', port))
    s.listen(5)
    SOCKETS.append(s)

# Catch the incoming TCP connections
while True:
    print("Waiting for connections")
    for socket in SOCKETS:
        conn, addr = socket.accept()
        print(f"Connection established at {addr}")
        CONNECTIONS.append(conn)

    # HAM
    counter = 0
    while True:
        ham = 0
        try:
            for filename in random.sample(HAM_DIR, NUM_HAM):
                with open(f'../dataset/newham/{filename}', 'r', errors='ignore') as file:
                    # read outside loop, cause multiple read calls on the same file return null
                    mail = file.read()
                    # this loop sends the same mail twice, but through different ports
                    for conn in CONNECTIONS:
                        try:
                            conn.sendall((mail + '\n').encode('utf-8'))
                        except UnicodeDecodeError:
                            print(f'{filename} could not be sent')
                        sleep(1/RATE)
                        counter += 1
                        ham += 1
            print(f"Sent {ham} ham mails")
        except BrokenPipeError:
            print("Execution interupted.")
            print(f"Sent only {ham} ham emails.")
            print(f"Sent a total of {counter} emails.")
            break
        
        # SPAM
        spam = 0
        try:
            for filename in random.sample(SPAM_DIR, NUM_SPAM):
                with open(f'../dataset/newspam/{filename}', 'r', errors='ignore') as file:
                    mail = file.read()
                    for conn in CONNECTIONS:
                        try:
                            conn.sendall((mail + '\n').encode('utf-8'))
                        except UnicodeDecodeError:
                            print(f'{filename} could not be sent')
                        sleep(1/RATE)
                        counter += 1
            print(f"Sent {spam} spam mails.")
        except BrokenPipeError:
            print("Execution interupted.")
            print(f"Sent {spam} spam emails.")
            print(f"Sent a total of {counter} emails.")
            break

    print(f"{counter} emails sent until now.")
    print("Closing connections.")
    for conn in CONNECTIONS:
        conn.close()
