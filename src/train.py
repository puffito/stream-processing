import re
from collections import Counter
from time import perf_counter

import nltk
import numpy as np
import pandas as pd
from nltk.corpus import stopwords, wordnet

from probability import Probability

LAPLACE_SMOOTHING = 1
DATA_LOCATION = 'emails.csv'

try:
    dataset = pd.read_csv(DATA_LOCATION)
    print("Imported DB")
except(FileNotFoundError):
    print("Dataset could not be found")
    exit(-1)

# Balance Dataset - random state ensures reproducibility of results
ham = dataset[dataset.spam == 1]
new_spam = dataset[dataset.spam == 0].sample(dataset[dataset.spam == 1].shape[0], random_state=1)
dataset = pd.concat([ham, new_spam])

# Get number of total, spam and ham email
number_emails, _ = dataset.shape
number_ham_emails, _ = dataset[dataset.spam == 0].shape
number_spam_emails, _ = dataset[dataset.spam == 1].shape

print("Optimizing")
# Drop duplicate, null and non valid entries in dataset
dataset.drop_duplicates(inplace=True)
dataset = pd.DataFrame(dataset.dropna())

# Remove "Subject :" part from each email
dataset['text'] = dataset['text'].map(lambda text: text[9:])

# Strip special characters and numbers
dataset['text'] = dataset['text'].map(
    lambda text: re.sub('[^a-zA-Z]+', ' ', text))

# Turn each word to lowercase and split the words of each message into a list.
dataset['text'] = dataset['text'].map(lambda text: (text.lower()).split())

# Remove commonly used words from data set using stopwords
# set with stopwords to use in list comprehension
stop_words = set(stopwords.words('english'))
dataset['text'] = dataset['text'].map(
    lambda text: [word for word in text if not word in stop_words])

# Strip each word down to its lemma
wnl = nltk.WordNetLemmatizer()
dataset['text'] = dataset['text'].apply(lambda text: [wnl.lemmatize((wnl.lemmatize(word, pos='n')), pos='v') for word in text])

# Get number of total, spam and ham email
number_emails, _ = dataset.shape
number_ham_emails, _ = dataset[dataset.spam == 0].shape
number_spam_emails, _ = dataset[dataset.spam == 1].shape

# Split training dataset into with spam and one with ham words and in how many emails they appear
dataset_ham, dataset_spam = [dataframe for _,
                                dataframe in dataset.groupby(dataset['spam'])]
print("Creating Dataframes")
list_spam = []
for sublist in dataset_spam['text']:
    for word in sublist:
        list_spam.append(word)
counter_spam = Counter(list_spam)

# Create ham counter
list_ham = []
for sublist in dataset_ham['text']:
    for word in sublist:
        list_ham.append(word)
counter_ham = Counter(list_ham)


# Create a sorted and indexed pandas Dataframe containg the words and their counts
ham = pd.DataFrame.from_dict(data=counter_ham, orient='index').reset_index().\
    rename(columns={'index': 'word', 0: 'count'}).sort_values(
        by=['word']).reset_index(drop=True)
spam = pd.DataFrame.from_dict(data=counter_spam, orient='index').reset_index().\
    rename(columns={'index': 'word', 0: 'count'}).sort_values(
        by=['word']).reset_index(drop=True)

number_ham_words, _ = ham.shape
number_spam_words, _ = spam.shape
number_unique_words = len(counter_ham | counter_spam)

# for Bayes' algorithm, total number of ham-word frequencies
ham_count = sum(counter_ham.values())
spam_count = sum(counter_spam.values())

print("Calculating Probabilities")
# Calculate probabilities to hopefully save time
ham['prob'] = (ham['count'] + LAPLACE_SMOOTHING) / \
    (LAPLACE_SMOOTHING*number_unique_words + ham_count)
spam['prob'] = (spam['count'] + LAPLACE_SMOOTHING) / \
    (LAPLACE_SMOOTHING*number_unique_words + spam_count)
missing_probability = LAPLACE_SMOOTHING / \
    (LAPLACE_SMOOTHING*number_unique_words + spam_count)

mprob_email_spam = Probability(number_spam_emails / number_emails)
mprob_email_ham = Probability(number_ham_emails / number_emails)
