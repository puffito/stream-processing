from math import log10


class Probability:
    """Represents a probability, split into its value and its order of magnitude

    Parameters:
    -----------
        order : {int}
        The order of magnitude of the given probability value. 
        Example: 342 has an order of 2, 0.35 has an order of -2

        value : (float)
        The value of the probability itself. The first digit is converted automatically
        to be contained between 1 and 9.
        Example: 0.0456 gets converted to value 4.56

    Attributes
    -----------
        value : (float)
        The current value of the probability expressed in the 0-th order of magnitude.

        original_value : (float)
        The original value of the Probability object upon instantiation, converted
        to its 0-th order of magnitude

        order : (int)
        The current order of magnitude of the probability.

        original_order : (int)
        The original order of magnitude of the Probability object upon instantiaton.
    """

    def __init__(self, value):
        if (int(log10(value) < 0)):
            self.order = self.original_order = int(log10(value) - 1)
        else:
            self.order = self.original_order = int(log10(value))
        self.value = self.original_value = value / 10 ** self.order

    def multiply(self, number):
        """Multiplies a Probability instance with a number given. 

        Args:
            number (float): The number, which the Probability instance will be
            multiplied with.
        """
        try:
            if int(log10(number)) < 0:
                number_order = int(log10(number) - 1)
            else:
                number_order = int(log10(number))
        except Exception as e:
            # if somehow number is zero
            print(f"Exception {e} thrown")
            print(f"Number given was: {number}, cannot calculate its logarith")
            exit(-1)
            
        self.order = self.order + number_order
        self.value = self.value * (number / 10 ** number_order)
        if self.value >= 10:
            self.value /= 10
            self.order += 1
        if self.value < 1:
            self.value *= 10
            self.order -= 1

    def calculate_q(self, Probability):
        """calculate_q Divides Probability with another one and returns
        0 1 or 2      

        Arguments:
        ----------
            Probability : {Probability}
            An instance of the Probability class

        Returns:
        --------
            int -- Returns 0 if first Probability is bigger, 1 if second Probability is bigger,
            2 if the same
        """
        value = self.value / Probability.value
        order = self.order - Probability.order
        # If order is positive, nominator is bigger
        if order != 0:
            if order > 0:
                return 0
            else:
                return 1
        # In the off-case order is zero, check the value
        else:
            if value > 1:
                return 0
            elif value < 1:
                return 1
            else:
                return 2

    def reset(self):
        self.value = self.original_value
        self.order = self.original_order
