import os
import re
import signal
import sys
import time

from pyspark import SparkConf, SparkContext
from pyspark.streaming import StreamingContext

from probability import Probability
from train import (
    ham, missing_probability, mprob_email_ham, mprob_email_spam, spam,
    stop_words, wnl)

#! program must be run from src/ directory!
# create spark context and streaming context
conf = SparkConf().setMaster("local[3]").setAppName("Streaming Classification").set(
    "spark.executor.memory", "2g").set("spark.executor.heartbeatInterval", "5s")
sc = SparkContext(conf=conf)
ssc = None
while ssc is None:
    try:
        ssc = StreamingContext(sc, 3)
        ssc.sparkContext.setLogLevel('ERROR')
    except Exception:
        print("Trying to set up spark streaming context again.")
        time.sleep(1)

# create 2 accumulators, to keep track of spam and ham emails
spam_acc = sc.accumulator(0)
ham_acc = sc.accumulator(0)

# monitoring socket
lines = ssc.socketTextStream("localhost", 9999)

# break down the stream into optimized words
words = lines.flatMap(lambda line:
                      re.sub('[^a-zA-Z]+', ' ', line).
                      lower().
                      split()).\
    filter(lambda word: word not in stop_words).\
    map(lambda word: wnl.lemmatize((wnl.lemmatize(word, pos='n')), pos='v'))

def classify(rdd):
    """
    Classifies the given mail contained in an rdd.
    """
    mail = rdd.collect()
    if len(mail) > 0:
        for word in mail:
            # Calculate spam
            if spam['word'].isin([word]).sum() > 0:
                mprob_email_spam.multiply(
                    spam[spam.word == word].values[0][2])
            else:
                mprob_email_spam.multiply(missing_probability)
            # Calculate ham
            if ham['word'].isin([word]).sum() > 0:
                mprob_email_ham.multiply(ham[ham.word == word].values[0][2])
            else:
                mprob_email_ham.multiply(missing_probability)
        q = mprob_email_ham.calculate_q(mprob_email_spam)
        if q == 0:
            ham_acc.add(1)
            print('ham')
        else:
            spam_acc.add(1)
            print('spam')
        mprob_email_ham.reset()
        mprob_email_spam.reset()


words.foreachRDD(classify)

# Setting signal handling to terminate Dstream properly
def terminate_process(signalNumber, frame):
    print('Got SIGTERM/SIGINT,terminating the process')
    ssc.stop(stopGraceFully=True, stopSparkContext=True)
    print(f"HAM: {ham_acc.value}, SPAM: {spam_acc.value}")
    sys.exit()

signal.signal(signal.SIGTERM, terminate_process)
signal.signal(signal.SIGINT, terminate_process)

# start stream
print("Starting DStream with PID ", os.getpid())
ssc.start()
ssc.awaitTermination()
